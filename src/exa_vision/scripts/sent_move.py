#!/usr/bin/env python

from __future__ import print_function
import rospy
from std_msgs.msg import String
import roslib
import sys
import signal
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist

from cv_bridge import CvBridge, CvBridgeError

from distutils.version import LooseVersion

if LooseVersion(cv2.__version__).version[0] == 2:
	# Whatever OpenCV3 code
	print ("Using OpenCV 2")
else:
	# Whatever OpenCV3 code
	print ("Using OpenCV 3")

#set speed hardcoded
min_width_image = 0
max_width_image = 1349
is_detect_nonobstacle = True

speed = 1
move_publisher = rospy.Publisher('exa_robot/cmd_vel', Twist, queue_size=2)
move_msgs = Twist()

"""
image proccessing OpenCV method

"""

"""
ROS method
"""

def obstacle_detect_in():
    """
        fill with your fucntion return value true or false non obstacle detected and where the pixel in width image
    """
    is_detect = True
    width_pixel = 1
    return is_detect, width_pixel
    

def get_move(is_detect_non_obstacle, width_image_data, min_image_width, max_image_width):
    if is_detect_non_obstacle:
        if min_image_width <= width_image_data <= 167 or  1175 < width_image_data <= max_image_width : #back
            forward = -1
            right = 0
        elif 167 < width_image_data <= 503:	#left
            forward = 0
            right = -1
        elif 503 < width_image_data <= 839: #forward
            forward = 1
            right = 0
        elif 839 < width_image_data <= 1175: #right
            forward = 0
            right = 1
#   elif 83 < width_image_data <= 251: #back-left
#	forward = 1
#        right = -1
#   elif 419 < width_image_data <= 587: #forward-left
#	forward = 1
#        right = 1
#   elif 755 < width_image_data <= 923: #forward-right
#	forward = -1
#        right = 1
#   elif 1091 < width_image_data <= 1259: #back-right
#	forward = -1
#        right = -1
        else:
            raise AssertionError (width_image_data>max_image_width,"width size from image data should be not greater than 1349!")
    else:
        forward = 0
        right = 0
    direction_forward = speed * forward
    direction_right = -1 * speed * right
    return direction_forward, direction_right

def sent_to_robot():
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown(): #loop
        width_data = 520			#enter function to process image stream save to width_data (int)
        move_msgs.linear.y = 0
        move_msgs.linear.z = 0
        move_msgs.angular.x = 0
        move_msgs.angular.y = 0
        move_msgs.linear.x, move_msgs.angular.z = get_move(is_detect_nonobstacle, width_data, min_width_image, max_width_image)
        rospy.loginfo("forward : %s | right : %s, Time : %s, ", move_msgs.linear.x, -1 * move_msgs.angular.z, rospy.get_time())
        move_publisher.publish(move_msgs)
        rate.sleep()

"""
**************########**************
**********###############***********
******######################********
"""
    
def stop_move():
    move_msgs.linear.y = 0
    move_msgs.linear.z = 0
    move_msgs.angular.x = 0
    move_msgs.angular.y = 0
    move_msgs.linear.x,move_msgs.angular.z = 0,0
    rospy.loginfo("forward : %s | right : %s, Time : %s, ", move_msgs.linear.x, -1 * move_msgs.angular.z, rospy.get_time())
    rospy.loginfo("MOVE STOPPED")
    move_publisher.publish(Twist())
    rospy.sleep(1)

def keyboardInterruptHandler(signal, frame):
    print("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    stop_move()
    exit(0)

#signal.signal(signal.SIGINT, keyboardInterruptHandler)

if __name__ == '__main__':
    try:
	#class function to prepare sending

    #initialize
        rospy.init_node('sent_move_to_robot', anonymous=True)
        rospy.on_shutdown(stop_move)
	#sent to robot
        sent_to_robot()
        # rospy.spin()
        exit()
    except rospy.ROSInterruptException or KeyboardInterrupt:
        #pass
        stop_move()
