# CMake generated Testfile for 
# Source directory: /home/vox/final_project/exa_robot/src
# Build directory: /home/vox/final_project/exa_robot/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(exa_control)
subdirs(exa_base)
subdirs(exa_vision)
