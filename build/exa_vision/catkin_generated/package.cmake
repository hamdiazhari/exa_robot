set(_CATKIN_CURRENT_PACKAGE "exa_vision")
set(exa_vision_VERSION "0.0.0")
set(exa_vision_MAINTAINER "marlonamadeus <marlonamadeus@todo.todo>")
set(exa_vision_PACKAGE_FORMAT "2")
set(exa_vision_BUILD_DEPENDS "roscpp" "std_msgs" "geometry_msgs" "pcl_ros" "pcl_conversions" "rtabmap_ros")
set(exa_vision_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs" "geometry_msgs" "pcl_ros" "pcl_conversions" "rtabmap_ros")
set(exa_vision_BUILDTOOL_DEPENDS "catkin")
set(exa_vision_BUILDTOOL_EXPORT_DEPENDS )
set(exa_vision_EXEC_DEPENDS "roscpp" "std_msgs" "pcl_ros" "pcl_conversions" "rtabmap_ros")
set(exa_vision_RUN_DEPENDS "roscpp" "std_msgs" "pcl_ros" "pcl_conversions" "rtabmap_ros" "geometry_msgs")
set(exa_vision_TEST_DEPENDS )
set(exa_vision_DOC_DEPENDS )
set(exa_vision_URL_WEBSITE "")
set(exa_vision_URL_BUGTRACKER "")
set(exa_vision_URL_REPOSITORY "")
set(exa_vision_DEPRECATED "")